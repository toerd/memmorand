package memmorand;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class Card extends JLabel implements Comparable<Card> {
	private String picturePath;
	private boolean flipped = false;
	private int num;
	private boolean matched = false;
	
	public Card(int num) {
		super("X");
		this.num = num;
	}
	
	public Card(int num, String path) {
		this(num);
		this.picturePath = path;
	}

	@Override
	public int compareTo(Card o) {
		return this.getNum() - o.getNum();
	}

	public int getNum() {
		return this.num;
	}
	
	public String getPicturePath() {
		return this.picturePath;
	}
	
	public boolean toggleFlip() {
		// if card is flipped -> unflip -> return new value
		if(!this.matched) {
			this.flipped = (this.flipped) ? false : true;
		}
		return this.flipped;
	}
	
	public boolean isFlipped() {
		return this.flipped;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}
}
