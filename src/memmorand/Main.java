package memmorand;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class Main extends JFrame {
	private List<Card> cardList = new ArrayList<>();
	private int cardCount;
	private Card lastFlippedCard;
	private int flippedCards = 0;

	public Main(int cardCount) {
		this.cardCount = cardCount;
		JPanel contentPane = new JPanel();
		this.setContentPane(contentPane);
		this.setLayout(null);

		contentPane.setLayout(new GridLayout(4, 4));

		for (int i = 0; i < this.cardCount; i++) {
			Card memLbl = new Card((i % 2 == 0 ? i : i - 1) / 2);
			cardList.add(memLbl);
		}

		for (Card c : cardList) {
			c.setHorizontalAlignment(SwingConstants.CENTER);
			c.setPreferredSize(new Dimension(100, 100));

			c.addMouseListener(new MouseListener() {
				public void mouseClicked(MouseEvent e) {
					if (c.isMatched())
						return;
					c.setText(Integer.toString(c.getNum()));
					System.out.println("CLICKED");
					if (flippedCards != 0) {
						if (!checkCards(c)) {
							System.out.println("FUCKING WRONG");
							// flip both cards back
							c.toggleFlip();
							lastFlippedCard.toggleFlip();
							c.setText("X");
							lastFlippedCard.setText("X");
						} else {
							lastFlippedCard = null;
						}
					} else {
						lastFlippedCard = c;
						flippedCards++;
						// update cardCount -> currently in chackCards func
					}
				}

				public void mousePressed(MouseEvent e) {
				}

				public void mouseReleased(MouseEvent e) {
				}

				public void mouseEntered(MouseEvent e) {
				}

				public void mouseExited(MouseEvent e) {
				}
			});

			contentPane.add(c);
		}

		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public boolean checkCards(Card currCard) {
		boolean result = false;
		System.out.println("ZWEITE KARTE GEFLIPPED");
		if (this.lastFlippedCard.compareTo(currCard) == 0) {
			result = true;
			this.lastFlippedCard.setMatched(true);
			currCard.setMatched(true);
			this.cardCount -= 2;
			if (this.cardCount <= 0) {
				System.out.println("Du hast gewonnen! Juhu!");
			}
		}
		this.flippedCards = 0;
		return result;
	}

	public static void main(String[] args) {
		new Main(16);
	}

}
